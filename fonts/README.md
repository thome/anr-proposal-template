
# Using calibri fonts

if you want to use calibri fonts, you need to copy them from official
licensed microsoft products. From the microsoft windows operating system
for example, you should copy the files named `calibri*` in the folder
`C:\Windows\Fonts`, and place them in the `fonts/` subdirectory (where
the file that you are currently reading resides).

```
> dir C:\Windows\Fonts\calib*
    Directory: C:\Windows\Fonts
Mode                 LastWriteTime         Length Name                         
----                 -------------         ------ ----                         
-a----         12/7/2019  10:08 AM        1648120 calibri.ttf                  
-a----         12/7/2019  10:08 AM        1613668 calibrib.ttf                 
-a----         12/7/2019  10:08 AM        1196672 calibrii.ttf                 
-a----         12/7/2019  10:08 AM        1438808 calibril.ttf                 
-a----         12/7/2019  10:08 AM        1061248 calibrili.ttf                
-a----         12/7/2019  10:08 AM        1159032 calibriz.ttf                 

```

Once you've done that, you can compile your document with the `xelatex`
processor (assuming you have it installed), and you should get the
calibri fonts.
