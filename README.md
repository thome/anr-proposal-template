
# Template for ANR proposals

This file is mostly the work of Vincent Labatut, with some minor edits.
The piece of work that I used as a base is [here](https://fr.overleaf.com/latex/templates/unofficial-template-for-anr-proposals/yqgzsxkzrqkw)

To use this in your project:

```
git submodule add https://gitlab.inria.fr/thome/anr-proposal-template anr
```

You can the copy the file `anr/example/anr_main.tex` and use it as a
base.

Presently, the package assumes that you will be using biber/biblatex for
your bibliography, and that your reference database is in the file
`anr_biblio.bib`. If this is a hassle to you, pass the `bibtex` option to
the package, so that `bibtex` is used instead. (e.g. with
`\documentclass[bibtex]{and/proposal}`)

# Example

if you cd to the subdirectory `example` (or `anr/example` from the top of
your git repository), you have a working example to play with. In fact
you even have two examples. One of them is called `model.tex`, and
strives to keep content that is as close as possible to the official
document that provides instructions.

# Margins

The official document that provides instructions has 2.5cm margins, but
says at the same time that margins should be 2cm. Here we use 2cm. Feel
free to adjust.

# Use of calibri fonts

The official document that provides instructions recommends calibri
fonts, which of course are available only with licensed microsoft
products. It _is_ possible, though, to use ttf fonts from latex, using
xelatex.

You'll need to get a copy of the calibri fonts in the first place,
obviously. Assuming you have access to a Windows installation, you can
copy the files named `calib*` in the folder `C:\Windows\Fonts`, and place
them in the `fonts/` subdirectory.

Once you've done that, you can compile your document with the `xelatex`
processor (assuming you have it installed), and you should get the
calibri fonts.

# Controlling hyperlink formatting.

If you wish to control the formatting of hyperlinks via options to be
passed to the `hyperref` package, you may do so by including the
following code before `\begin{document}` (the example below actually
reproduces the defaults):

```
\def\hyperrefoptions{%
                bookmarks=true, bookmarksnumbered=true, bookmarksopen=true,
                unicode=true, colorlinks=true, linktoc=all,
                linkcolor=anrcolor, citecolor=anrcolor,
                filecolor=anrcolor, urlcolor=anrcolor,
                pdfstartview=FitH
                }
```

# CV template

ANR suggests a template for the CV formatting. It does not seem to be
well adapted to academic CVs, and it does not seem mandatory to follow
it. Yet, if you wish to do so, the CV template here can be convenient.

# Contributing

Of course, feedback and changes are welcome. If you want to contribute,
please read
[this](https://lists.gforge.inria.fr/pipermail/cado-nfs-discuss/2020-October/001318.html)
; since I'm the same person, the same general motto applies. Send me an
e-mail, I'll definitely approve your request for access.
