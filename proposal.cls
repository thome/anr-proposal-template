%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Unofficial class for ANR Project proposals
%%
%% v1.2.1 - 25/02/2020: modification to fit the changes in packages pgfgantt, thanks to Nicolas Marchand
%% v1.2   - 02/03/2019: minor adjustments to match the 2019 version of the template
%% v1.1   -    04/2018: minor adjustments in the class
%% v1     -    03/2018: first draft
%%
%% Vincent Labatut 2018-20
%% vincent.labatut@univ-avignon.fr
%%
%% Please tell me if you find any error.
%% And if you use this model for your ANR project: 
%% I only take 8% of your accepted budget ;)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% init class
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{anr/proposal}
\LoadClass[a4paper,11pt,final]{article}	% based on the existing latex class
% \errorcontextlines 10000				% uncomment only when debugging

\RequirePackage{xkeyval}

\newif\if@anr@bibtex
\DeclareOption{bibtex}{\@anr@bibtextrue}
\DeclareOption{biblatex}{\@anr@bibtexfalse}
\DeclareOptionX{hyperref}{\def\@hyperref@options{#1}}
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% encoding and stuff
\RequirePackage[english]{babel}				% language package
\RequirePackage[utf8]{inputenc}				% use source file containing diacritics
\RequirePackage[T1]{fontenc}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% font settings
\RequirePackage{ifxetex}

\ifxetex
\usepackage{fontspec}
\setmainfont[Path=anr/fonts/,
    BoldItalicFont=calibriz.ttf,
    BoldFont      =calibrib.ttf,
    ItalicFont    =calibrii.ttf]{calibri.ttf}
\else
\RequirePackage[babel=true, kerning=true]{microtype} % typographical refinements
\RequirePackage{lmodern}			     % uses better fonts (?)
\renewcommand{\familydefault}{\sfdefault}	% use a sans-serif font as in the official template
\fi

\RequirePackage{eurosym}					% euro symbol (€): very important!


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% image settings
% \RequirePackage[pdftex]{graphicx}			% improves \includegraphics
\RequirePackage{graphicx}			% improves \includegraphics


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% space-related settings
\RequirePackage[top=1.25cm, 					% 2cm margins, as specified in the official MS Word template
	bottom=1.25cm,
    left=2cm, 
    right=2cm,
    includehead,includefoot]{geometry}					% changes document margins
\RequirePackage{setspace}					% controls spacing between lines


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% misc
\RequirePackage[english]{varioref} 			% improves cross-refs
\RequirePackage{lipsum}						% filler text


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% header/footer settings
\RequirePackage{lastpage}					% computes the number of pages
\RequirePackage{fancyhdr}					% changes headers and setters
% \RequirePackage{titling}					% better control over the title
\newcommand{\setHeaders}{					% macro initializing the headers and footers
	\pagestyle{fancy}
	\fancyhf{}
	\chead{
\rowcolors{1}{headcolor}{headcolor}
\setlength{\arrayrulewidth}{1pt}
\vbox to 0pt{\vskip-\ht\strutbox\begin{tabular}{p{0.18\linewidth}|p{0.36\linewidth}|p{0.18\linewidth}|p{0.175\linewidth}}
    \arrayrulecolor{white}\hline
    {\Large\strut\bfseries\textcolor{anrcolor}{AAPG\myacyear}} &
    \multicolumn{2}{l|}{\Large\strut\textbf{\myacronym}} & \myinstrument \\
    \arrayrulecolor{white}\hline
    \textcolor{anrcolor}{Coordinated by:} & \myinvestigator & \myduration & \myfunding \\
    \arrayrulecolor{white}\hline
    \multicolumn{4}{l}{\mycommittee}
\end{tabular}\vss}}
	\rfoot{\textcolor{anrcolor}{\thepage\ / \pageref*{LastPage}}}
%	\renewcommand\headrule{					% color and thickness of the header rule
%    	{\color{black}\hrule width \hsize}}
    \renewcommand\headrule{}
    \renewcommand\footrule{}					% color and thickness of the header rule
}
\advance\headsep4pt
\headheight 40pt
\setlength{\footskip}{15pt}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% color settings
\RequirePackage[table]{xcolor}					% defines custom colors
\RequirePackage{colortbl}                       % to change the color of the table lines
% \let\normalcolor\relax							% prevent automatic restoration to black
\definecolor{headcolor}{HTML}{b5dce7}
\definecolor{anrcolor} {HTML}{30839a}	% color for "ANR" (in the header)
\definecolor{partnercol1}{HTML}{e41a1c}			% partner colors (taken from colorbrewer2.org)
\definecolor{partnercol2}{HTML}{4daf4a}			% green
\definecolor{partnercol3}{HTML}{984ea3}			% purple
\definecolor{partnercol4}{HTML}{ff7f00}			% orange
\definecolor{partnercol5}{HTML}{f781bf}			% pink
\definecolor{partnercol6}{HTML}{377eb8}			% blue
\definecolor{partnercol7}{HTML}{a65628}			% brown
\definecolor{partnercol8}{HTML}{ffff33}			% yellow
% NOTE: you must define additional colors in the (unlikely) case where you have more than 8 partners


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% diagram settings
\RequirePackage{tikz}							% programmatically defines drawings
\usetikzlibrary{arrows}							% configures arrow tips
\usetikzlibrary{arrows.meta}					% necessary to draw graphs
\usetikzlibrary{positioning} 					% ease relative positioning


\RequirePackage{anr/pgfgantt-custom}		    % gantt diagrams
\newcommand\ganttbarbis[5]{\ganttbar{#1}{#3}{#4}\ganttbar[inline, bar/.append style={fill=#5}, bar label font=\footnotesize]{#2}{#3}{#4}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% hyperlinks settings
\ifx\hyperrefoptions\undefined
\def\hyperrefoptions{%
		bookmarks=true, bookmarksnumbered=true, bookmarksopen=true,
		unicode=true, colorlinks=true, linktoc=all, %linktoc=page
		linkcolor=anrcolor, citecolor=anrcolor,
                filecolor=anrcolor, urlcolor=anrcolor,
                pdfstartview=FitH}
\fi
\RequirePackage[\hyperrefoptions]{hyperref} % enables hyperlinks
\RequirePackage{url}							% properly displays urls


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% table settings
\RequirePackage{multirow}						% cells spanning several rows
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}	% thicker line for the tables


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% mathematical settings
\RequirePackage{amsmath}						% extended mathematical functions
\RequirePackage{amsthm}							% even more advanced math functions
\RequirePackage{amssymb}  						% additional math symbols
\newtheorem{theorem}{Theorem}					% those are not really needed here, but whatever
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{Hypothesis}[theorem]{Hypothesis}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{definition}[theorem]{Definition}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% bibliographic settings
\usepackage{doi}						% displays DOIs in the bibliography
\usepackage[autostyle]{csquotes}		% don't remember why I put this
\if@anr@bibtex\else
\usepackage[maxcitenames=2,				% two authors max in the text (only for author-year)
	maxbibnames=99,						% list all authors in the reference list
	style=numeric,						% numeric style for reference list
	citestyle=numeric-comp,				% numeric style for citations
%	style=authoryear,					% NOTE: if you want the author-year style instead,
%	citestyle=authoryear-comp,			% uncomment these 2 lines and comment both ones above
%	backref=true,						% includes a page backlink in the reference list
%	dashed=false,						% displays all author names (only for authoryear)
    backend=biber]						% uses biber to compile (instead of bibtex)
{biblatex}								% uses biblatex (instead of bibtex)
\renewcommand*{\bibfont}{\footnotesize} % NOTE: font size of the bibliographical section text
\fi


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% caption settings
\RequirePackage{caption}					% caption customisation
\captionsetup{
	labelfont={color=anrcolor, bf, small},	% font(sf), color, bold, size of the caption label
    textfont={small},						% font(sf) and size of the caption text itself
    labelsep=period,						% separator between label and text
    margin=10mm								% left/right margins
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% custom equations
%	\makeatletter								% change equation numbers
\def\tagform@#1{\maketag@@@{(\color{anrcolor}\textbf{#1}\normalcolor)\@@italiccorr}}
%	\makeatother


%%%%%%%%%%%%%%%%%%%%%%%%%
% bullet list
%\renewcommand{\FrenchLabelItem}{\textbullet}	% use bullets even in French
\usepackage{enumitem}							% gives more control over lists
\setlist{nolistsep}								% changes spacing between list items


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% macros used to set the project details
\def\title#1{\gdef\@title{#1}\gdef\mytitle{#1}} % full title of the project
\def\subtitle#1{\gdef\mysubtitle{#1}}           % subtitle of the project (optional)
\def\acronym#1{\gdef\myacronym{#1}}			    % acronym of the project
\def\committee#1{\gdef\mycommittee{#1}}		    % scientific evaluation committee
\def\instrument#1{\gdef\myinstrument{#1}}	    % funding instrument
\def\acyear#1{\gdef\myacyear{#1}}				% year of the call
\def\investigator#1{\gdef\myinvestigator{#1}}   % principal investigator
\def\duration#1{\gdef\myduration{#1}}           % duration of the project
\def\funding#1{\gdef\myfunding{#1}}             % requested funding


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% custom section headers
\renewcommand\thesection{\Roman{section}}	% sets the weird section numbering of the official template
\renewcommand\thesubsection{\thesection.\alph{subsection}}
\renewcommand\thesubsubsection{\thesubsection.\arabic{subsubsection}}
\RequirePackage{titlesec}
\titleformat{\section}
        {\color{anrcolor}\normalfont\bfseries\fontsize{18pt}{20pt}\selectfont}
    {\thesection.}
    {1em}
    {}
\titleformat{\subsection}
	{\color{anrcolor}\normalfont\large\bfseries}
    {\thesubsection.}
    {1em}
    {}
\titleformat{\subsubsection}
	{\color{anrcolor}\normalfont\normalsize\bfseries}
    {\thesubsubsection.}
    {1em}
    {}
\newcommand{\sectionn}[1]{
	\phantomsection
	\addcontentsline{toc}{section}{#1}
	\section*{#1}
    }
\newcommand{\subsectionn}[1]{
	\phantomsection
	\addcontentsline{toc}{subsection}{#1}
	\subsection*{#1}
    }
\newcommand{\subsubsectionn}[1]{
	\phantomsection
	\addcontentsline{toc}{subsubsection}{#1}
	\subsubsection*{#1}
    }
\setcounter{secnumdepth}{3}			% numbers subsubsections
\setcounter{tocdepth}{5}			% and includes them in the TOC

\parindent 0pt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% work packages and tasks
\newcounter{CntWP}					% WP counter
\setcounter{CntWP}{-1}				% NOTE: comment this line to number WPs starting from 1
\newcounter{CntT}[CntWP]			% task counter
\def\workpackagename{Work Package}
\def\taskname{Task}
% 20201026: kill explicit spacing below. It's better handled right from
% the \titlespacing* commands
\newcommand{\wpdef}[2]{%				% defines new WPs
	\refstepcounter{CntWP}\label{#1}%
    % \par\vspace{0.5cm}
	\phantomsection
	\addcontentsline{toc}{subsubsection}{\workpackagename~\arabic{CntWP}.~#2}%
    \noindent
    \subsubsection*{\workpackagename~\arabic{CntWP}:~#2}}

% This variant can be used inside itemizeenvironment, if that seems more
% convenient. We do not add toc entries in this case.
\newcommand{\wpdefinline}[2]{%
    \refstepcounter{CntWP}\label{#1}%
    {\normalfont\normalsize\bfseries\textcolor{anrcolor}{\workpackagename~\arabic{CntWP}:~#2.}\hskip.5em}}


\newcommand{\tdef}[2]{%				% defines new tasks
	\refstepcounter{CntT}\label{#1}%
    % \par\vspace{0.25cm}
	\phantomsection
	\addcontentsline{toc}{paragraph}{\taskname~\arabic{CntWP}.\arabic{CntT}.~#2}%
    % \noindent\hspace{0.5cm}
    {\normalfont\normalsize\bfseries\itshape\textcolor{anrcolor}{\taskname~\arabic{CntWP}.\arabic{CntT}:~#2}}\newline}%
\renewcommand*\theCntT{\theCntWP.\arabic{CntT}}

\newcommand{\wpref}[1]{{\textcolor{anrcolor}{\workpackagename~\ref{#1}}}}
\newcommand{\taskref}[1]{{\textcolor{anrcolor}{\taskname~\ref{#1}}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% maketitle macro
\renewcommand{\maketitle}
{	\setHeaders									% sets up the headers and footers	
	\hypersetup{pdftitle={\mytitle}}			% adds title to PDF properties
    \begin{center}
    	\vspace*{-0.25cm}						% adds a bit of space before the title
        \huge\textbf{\mytitle}				    % places the title
        \ifdefined\mysubtitle
            \\\Large\textbf{\mysubtitle}		% places the subtitle
        \fi%
    \end{center}
    \global\@topnum\z@							% prevents floats from going above the title
}
